package pl.com.sda.homework;

public class Zadanie7 {

    public static void main(String[] args) {
        drawTriangle(3);
    }

    public static void drawTriangle(int n) {

        String s = "";

        for (int i = 0; i < n; i++) {

            s = s + '#';

            System.out.println(s);
        }
    }
}