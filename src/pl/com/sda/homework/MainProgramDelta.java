package pl.com.sda.homework;

import java.util.Scanner;

public class MainProgramDelta {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Write first number:");
        double a = scanner.nextDouble();
        System.out.println("Write second number:");
        double b = scanner.nextDouble();
        System.out.println("Write third number:");
        double c = scanner.nextDouble();

        double delta = b * b - 4 * a * c;
        System.out.println("Delta = " + delta);

        double x1 = (-b + Math.sqrt(delta)) / (2 * a);

        double x2 = (-b - Math.sqrt(delta)) / (2 * a);

        double x3 = -b / (2 * a);

        if (delta > 0) {
            System.out.println("Miejsce zerowe x1 = " + x1 + "\n" + "Miejsce zerowe x2 = " + x2);
        } else if (delta == 0) {
            System.out.println("Miejsce zerowe = " + x3);
        } else {
            System.out.println("Brak rozwiązań");
        }
    }
}
