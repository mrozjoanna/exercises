package pl.com.sda.homework.exercisePets;

public interface WaterBeing {

    boolean breathUnderWater();

}
