package pl.com.sda.homework.exercisePets;

public class Cat extends Pet{

    public Cat(String covering) {
        super(covering);
    }

    @Override
    public void makeSound() {
        System.out.println("MiauMiau!");
    }

    @Override
    public void getCountOfLegs() {

    }
}