package pl.com.sda.homework.exercisePets;

public class Fish extends Pet implements WaterBeing {

    public Fish(String covering) {
        super(covering);
    }

    @Override
    public void makeSound() {
        System.out.println("PlumPlum");
    }

    @Override
    public void getCountOfLegs() {
    }

    @Override
    public boolean breathUnderWater() {
        return true;
    }
}
