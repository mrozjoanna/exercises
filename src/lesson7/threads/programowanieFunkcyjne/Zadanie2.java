package lesson7.threads.programowanieFunkcyjne;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Zadanie2 {
    public static void main(String[] args) {
        List<String> txt = Arrays.asList(null, "Ala", "samochód", "kot", "aleksandra", "pies", "azot");
        List<String> target = txt
                .stream()
                .filter(n -> n!= null && (n.charAt(0) == 'a' || n.charAt(0)=='A'))
                .map (n -> Character.toUpperCase(n.charAt(0)) + n.substring(1).toLowerCase())
                .collect(Collectors.toList());


        for (String string : target)
            System.out.println(string);
    }
}

